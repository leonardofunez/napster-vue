import Vue from 'vue'
import Router from 'vue-router'

import Login from './views/Login.vue'
import Register from './views/Register.vue'

import Home from './views/Home.vue'
import Explore from './views/Explore.vue'

import Playlists from './views/Playlists.vue'
import Playlist from './views/Playlist.vue'
import Album from './views/Album.vue'
import Artist from './views/Artist.vue'
import Genre from './views/Genre.vue'

import FavouritePlaylists from './views/FavouritePlaylists.vue'
import FavouriteAlbums from './views/FavouriteAlbums.vue'
import FavouriteTracks from './views/FavouriteTracks.vue'

Vue.use(Router)

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	scrollBehavior() {
		return { x: 0, y: 0 };
	},
	routes: [
		{ path: '/', name: 'home', component: Home },
		{ path: '/login', name: 'login', component: Login },
		{ path: '/register', name: 'register', component: Register },
		{ path: '/explore', name: 'explore', component: Explore },
		
		{ path: '/playlists', name: 'playlists', component: Playlists },
		{ path: '/playlists/:id', name: 'playlist', component: Playlist },
		{ path: '/albums/:id', name: 'album', component: Album },
		{ path: '/artists/:id', name: 'artist', component: Artist },
		{ path: '/genres/:id', name: 'genre', component: Genre },
		
		{ path: '/favourite-playlists', name: 'favourite-playlists', component: FavouritePlaylists },
		{ path: '/favourite-albums', name: 'favourite-albums', component: FavouriteAlbums },
		{ path: '/favourite-tracks', name: 'favourite-tracks', component: FavouriteTracks },
	]
})
